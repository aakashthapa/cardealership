from django.db import models
from django.urls import reverse

# Create your models here.
class SalesPerson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("sales_person", kwargs={"pk": self.pk})

class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField(null=False)
    phone_number = models.CharField(max_length=15, null=False)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salespeople",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()

def get_api_url(self):
    return reverse("api_show_sale", kwargs={"pk": self.pk})

def __str__(self):
    return f"{self.automobile} - {self.customer} - {self.salesperson}"

