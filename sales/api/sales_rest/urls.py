from django.urls import path
from .views import api_list_salesperson, api_detail_salesperson, api_list_customer , api_detail_customer, api_list_sales, sale_details

urlpatterns = [
    path("salespeople/", api_list_salesperson, name="api_list_salesperson"),
    path('salespeople/<int:id>/', api_detail_salesperson, name="api_detail_salesperson"),
    path('customers/', api_list_customer, name="api_list_customer"),
    path('customers/<int:id>/', api_detail_customer, name="api_detail_customer"),
    path('sales/', api_list_sales, name="api_list_sales"),
    path('sales/<int:id>/', sale_details, name="sale_details"),
]
