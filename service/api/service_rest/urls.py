from django.urls import path

from service_rest.views import appointment_list, delete_app, technician_list, update_app, delete_tech

urlpatterns = [
    path("technicians/", technician_list, name="techs_list"),
    path("technicians/<int:tech_id>/", delete_tech, name="delete_tech"),
    path("appointments/", appointment_list, name="appointment_list"),
    path("appointments/<int:app_id>/", delete_app, name="delete_app"),
    path("appointments/<int:app_id>/cancel", update_app, name="cancel_app"),
    path("appointments/<int:app_id>/finish", update_app, name="finish_app")
]
