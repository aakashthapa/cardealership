import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder

from service_rest.models import Appointment, AutomobileVO, Technician

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "vip",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
        }
    


@require_http_methods(["GET", "POST"])
def technician_list(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechnicianListEncoder
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_tech(request,tech_id):
    count, _ = Technician.objects.filter(id=tech_id).delete()
    return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "POST"])
def appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        if AutomobileVO.objects.filter(vin=content["vin"]):
            content["vip"] = True
        try:
            tech = Technician.objects.get(id=content["technician"])
            content["technician"] = tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Tech Id"},
                status=400
            )
        date = content.pop("date")
        time = content.pop("time")
        content["date_time"] = f"{date} {time}"
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
    
@require_http_methods(["PUT"])
def update_app(request, app_id):

    content = json.loads(request.body)
    Appointment.objects.filter(id=app_id).update(**content)
    appointment = Appointment.objects.get(id=app_id)
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False
    )

@require_http_methods(["DELETE"])
def delete_app(request, app_id):
    count, _ = Appointment.objects.filter(id=app_id).delete()
    return JsonResponse({"deleted": count > 0})

