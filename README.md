# CarCar

Aakash Thapa

## Design
CarCar is a platform designed for documenting sales, services, and current inventory. It comprises several microservices developed in React, which interact with a Django backend API. The inventory microservice facilitates the addition of manufacturers, car models, and vehicles to the inventory. The service microservice enables the addition of technicians, appointment scheduling, viewing of scheduled appointments, and displaying the service history of cars linked to their VIN numbers. The sales microservice presents car sales, showcases sales records, and manages customers and salespeople. Additionally, the poller regularly polls the Automobile Value Object from the Inventory API at intervals of 60 seconds.

![IMAGE_DESCRIPTION](https://gitlab.com/DavidHelfer1616/project-beta/-/raw/main/DesignImage/carcardesign.png?ref_type=heads)

## How to run this Project 
To run this project, follow these steps:

1. Navigate to the directory where you want to run the project and clone the repository using the following command:

git clone https://gitlab.com/aakashthapa/cardealership.git

2. Change into the project directory:

cd project-beta

3. Run the following three commands in sequence:
* docker volume create beta-data
* docker-compose build
* docker-compose up

Note: If you're using macOS, you may see a warning about a missing environment variable OS. You can safely ignore this warning.

4. To access the API backend, you can use a tool like Insomnia to make HTTP requests.

5. To access the frontend, go to http://localhost:3000/ in your web browser, where the React app is hosted.


## Design

CarCar is made up of 3 microservices which interact with one another.

- **Inventory**
- **Services**
- **Sales**

## Integration - How we put the "team" in "team"

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
    "name": "Chrysler"
}
```
The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
# Sales Microservice

On the backend, the sales microservice has 4 models: AutomobileVO, Customer, SalesPerson, and Sale. Sale is the model that interacts with the other three models. This model gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.


## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8091/api/customers/
| Create a customer | POST | http://localhost:8091/api/customers/
| Show a specific customer | GET | http://localhost:8091/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name": "Mac",
	"last_name": "Donald",
	"address": "1212 Ocean Street",
	"phone_number": 9804357878
}
```
Return Value of Creating a Customer:
```
{
	"first_name": "Mac",
	"last_name": "Donald",
	"address": "1212 Ocean Street",
	"phone_number": 9804357878
}
```
Return value of Listing all Customers:
```
{
	"customers": [
		{
			"first_name": "Mac",
			"last_name": "Donald",
			"address": "1212 Ocean Street",
			"phone_number": "9804357878",
			"id": 1
		},
		{
			"first_name": "Arnold",
			"last_name": "Schwaz",
			"address": "2311 Alder Dr Houston TX",
			"phone_number": "9821452418",
			"id": 3
		}
	]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8091/api/salespeople/
| Salesperson details | GET | http://localhost:8091/api/salespeople/id/
| Create a salesperson | POST | http://localhost:8091/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8091/api/salespeople/id/


To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Ram",
	"last_name": "Sharma",
	"employee_id": 2
}
```
Return Value of creating a salesperson:
```
{
	"first_name": "Ram",
	"last_name": "Sharma",
	"employee_id": 2,
	"id": 3
}
```
List all salespeople Return Value:
```
{
	"salespersons": [
		{
			"first_name": "John",
			"last_name": "Rambo",
			"employee_id": "1",
			"id": 1
		},
		{
		    "first_name": "Ram",
			"last_name": "Sharma",
			"employee_id": 2,
			"id": 3
		},
		{
			"first_name": "Gary",
			"last_name": "Angelo",
			"employee_id": "1092",
			"id": 4
		}
	]
}
```
### Sales:
- the id value to show a salesperson's sales record is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sale | GET | http://localhost:8091/api/sales/
| Create a new sale | POST | http://localhost:8091/api/sales/
| Show sales details | GET | http://localhost:8091/api/sales/id/
| Delete specific sale | DELETE | http://localhost:8091/api/sales/id/

List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120188",
				"sold": true,
				"id": 2
			},
			"salesperson": {
				"first_name": "John",
				"last_name": "Rambo",
				"employee_id": "1",
				"id": 1
			},
			"customer": {
				"first_name": "Mac",
				"last_name": "Donald",
				"address": "1212 Ocean Street",
				"phone_number": "9804357878",
				"id": 1
			},
			"price": 25000,
			"id": 1
		},
			{
			"automobile": {
				"vin": "3N1AB7AP3EY291659",
				"sold": true,
				"id": 16
			},
			"salesperson": {
				"first_name": "Gary",
				"last_name": "Angelo",
				"employee_id": "1092",
				"id": 4
			},
			"customer": {
				"first_name": "Mac",
				"last_name": "Donald",
				"address": "1212 Ocean Street",
				"phone_number": "9804357878",
				"id": 1
			},
			"price": 35000,
			"id": 46
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
    "automobile": "1GTV2UEC7FZ290056",
    "salesperson": 3,
    "customer": 1,
    "price": 25000.00
}
```
Return Value of Creating a New Sale:
```
{
	"automobile": {
		"vin": "1GTV2UEC7FZ290056",
		"sold": true,
		"id": 5
	},
	"salesperson": {
		"first_name": "test",
		"last_name": "case",
		"employee_id": "2",
		"id": 3
	},
	"customer": {
		"first_name": "Mac",
		"last_name": "Donald",
		"address": "1212 Ocean Street",
		"phone_number": "9804357878",
		"id": 1
	},
	"price": 25000.0,
	"id": 29
}
```
Show sale details:
```
{
	"automobile": {
		"vin": "1C3CC5FB2AN120188",
		"sold": true,
		"id": 2
	},
	"salesperson": {
		"first_name": "John",
		"last_name": "Rambo",
		"employee_id": "1",
		"id": 1
	},
	"customer": {
		"first_name": "Mac",
		"last_name": "Donald",
		"address": "1212 Ocean Street",
		"phone_number": "9804357878",
		"id": 1
	},
	"price": 25000,
	"id": 1
}
```
# Service microservice

Hello and welcome to the wonderful world of service!!
As explained above, the service microservice is an extension of the dealership that looks to provide service repairs for your vehicle.

As automobiles are purchased, we keep track of the vin number of each automobile and you are able to receive the special perks of being a VIP!
As a VIP, you will receive free oil changes for life, complimentary neck massages while in our waiting room, and free car washes whenever you would like!

This area is going to be broken down into the various API endpoints (Fancy way of saying your web address url) for service along with the format needed to send data to each component.
The basics of service are as follows:
1. Our friendly technician staff
2. Service Appointments


### Technicians - The heart of what we do here at CarCar
(We are considering renaming, don't worry)

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:tech_id>/


LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.
```
Example:
{
	"technicians": [
		{
            "id": 1,
			"first_name": "Donald",
            "last_name": "Duck"
			"employee_id": "dduck",
			
		},
    ]
}
```

CREATE TECHNICIAN - What if we hired a new technician (In this economy even)? To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
```
{
	"first_name": "Liz",
    "last_name": "Stinkerton",
	"employee_id": lstinker
}
```
As you can see, the data has the same format. In this example, we just changed the "first_name" field from "Donald" to "Liz". We also assigned her the "employee_id" value of "2" instead of "1".
Once we have the data into your request, we just hit "Send" and it will create the technician "Liz". To verify that it worked, just select follow the "LIST TECHNICIAN" step from above to show all technicians.
With any luck, both Donald and Liz will be there.
Here is what you should see if you select "LIST TECHNICIAN" after you "CREATE TECHNICIAN" with Liz added in:
```
{
	"technicians": [
		{
            "id": 1,
			"first_name": "Donald",
            "last_name": "Duck"
			"employee_id": "dduck",
			
		},
		{
            "id": 2
			"first_name": "Liz",
            "last_name": "Stinkerton",
			"employee_id": 1,
			
		},
    ]
}
```

DELETE TECHNICIAN - If we decide to "go another direction" as my first boss told me, then we need to remove the technician from the system. To do this, you just need to change the request type to "DELETE" instead of "POST". Once they are "promoted to customer" they will no longer be in our page that lists all technicians.


And that's it! You can view all technicians, look at the details of each technician, and create technicians.
Remember, the "id" field is AUTOMATICALLY generated by the program. So you don't have to input that information. Just follow the steps in CREATE TECHNICIAN and the "id" field will be populated for you.
If you get an error, make sure your server is running and that you are feeding it in the data that it is requesting.
If you feed in the following:
```
{
	"first_name": "Liz",
	"employee_id": 3,
	"favorite_food": "Tacos"
}

You will get an error because the system doesn't know what what to do with "Tacos" because we aren't ever asking for that data. We can only send in data that Json is expecting or else it will get angry at us.

```


### Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | PUT | http://localhost:8080/api/serviceappointment/<int:id>/cancel
| Delete service appointment | PUT | http://localhost:8080/api/serviceappointment/<int:id>/finish
| Delete service appointment | DELETE | http://localhost:8080/api/serviceappointment/<int:id>


LIST SERVICE APPOINTMENT: This will return a list of all current service appointment.
This is the format that will be displayed.
Spoiler alert! Remember, the way that it is returned to you is the way that the data needs to be accepted. Remember, the "id" is automatically generated, so you don't need to input that.
Also, the "date" and "time" fields HAVE TO BE IN THIS FORMAT
```
{
	"service_appointment": [
		{
			"id": 1,
			"vin": "1222",
			"customer": "Barry",
			"date_time": "2021-07-14 12:30:00",
			"reason": "mah tires",
			"vip": false,
			"technician": {
                "first_name": "Liz",
                "last_name": "Stinkerton",
                "employee_id": "lstinkerton"
                }
		},
    ]
}
```

CREATE SERVICE APPOINTMENT - This will create a service appointment with the data input. It must be a "POST" request and follow this format:
```
		{
			"vin": "1222",
			"customer": "Gary",
			"date_time": "2021-07-11 12:30:00",
            "technician": "3", <-- "this is tech_id"
			"reason": "new car"
			
		}

```

UPDATE SERVICE APPOINTMENT - On each service appointment, you will have to option to update the status by clicking the button "cancel" or "finish". this action will send a "PUT" request to one of the two "PUT" request options above. The body of request must follow the template below:
```
		{
			"status": "cancelled" <-- or finished depending on url path
		}

```

DELETE SERVICE APPOINTMENT - Just input the "id" of the service appointment that you want to delete at the end of the url. For example, if we wanted to delete the above service history appointment for Barry
because we accidently input his name as "Gary", we would just enter 'http://localhost:8080/api/appointments/1' into the field and send the request. We will receive a confirmation message saying that
the service appointment was deleted.
