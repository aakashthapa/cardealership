import React, { useEffect, useState } from "react";

function ServiceList() {
    const [serviceList, setServiceList] = useState([])

    const getServiceList = async () => {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setServiceList(data.appointments)
        }
    }

    function getDate(date) {
        return new Date(date).toLocaleDateString()
    }

    function getTime(time) {
        return new Date(time).toLocaleTimeString()
    }

    async function statusCancel(service) {
        const locationUrl = `http://localhost:8080/api/appointments/${service.id}/cancel`;

        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ status: "cancelled" }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            getServiceList();
        }
    }

    async function statusFinish(service) {
        const locationUrl = `http://localhost:8080/api/appointments/${service.id}/finish`;

        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ status: "finished" }),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            getServiceList();
        }
    }


    useEffect(() => {
        getServiceList();
    }, []);

    return (
        <div className="container">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h1>Service Appointments</h1>
            </div>

            <table
                className="table table-bordered table-striped"
            >
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {serviceList.filter(service => service.status === "created").map((service) => (
                        <tr key={service.id}>
                            <td>{service.vin}</td>
                            <td>{service.vip ? "Yes" : "No"}</td>
                            <td>{service.customer}</td>
                            <td>{getDate(service.date_time)}</td>
                            <td>{getTime(service.date_time)}</td>
                            <td>{`${service.technician.first_name} ${service.technician.last_name}`}</td>
                            <td>{service.reason}</td>
                            <td>
                                <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                                    <button onClick={() => statusCancel(service)} type="button" className="btn btn-danger">Cancel</button>
                                    <button onClick={() => statusFinish(service)} type="button" className="btn btn-success">Finish</button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div >
    );
}

export default ServiceList