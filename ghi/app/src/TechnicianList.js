import React, { useEffect, useState } from "react";

function TechnicianList() {
    const [techList, setTechList] = useState([])

    const getTechList = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechList(data.techs)
        }
    }

    useEffect(() => {
        getTechList();
    }, []);

    return (
        <div className="container">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h1>Technicians</h1>
            </div>

            <table
                className="table table-bordered table-striped"
            >
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {techList.map((tech) => (
                        <tr key={tech.id}>
                            <td>{tech.employee_id}</td>
                            <td>{tech.first_name}</td>
                            <td>{tech.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div >
    );
}

export default TechnicianList