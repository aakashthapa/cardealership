import { useState } from "react"
import { useNavigate } from "react-router-dom"


function TechnicianForm() {

    const initialData = {
        first_name: '',
        last_name: '',
        employee_id: '',
    }

    const [formData, setFormData] = useState(initialData)
    const navigate = useNavigate()

    const handleFormChange = (event) => {
        const key = event.target.name
        const value = event.target.value
        setFormData({
            ...formData,
            [key]: value,
        })
        
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const locationUrl = 'http://localhost:8080/api/technicians/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if(response.ok){
            navigate("/technicianlist")
        }
        
    }

    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow p-4 mt-4">
                    <h1>Add Technician</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={formData.first_name} onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.last_name} onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.employee_id} onChange={handleFormChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default TechnicianForm