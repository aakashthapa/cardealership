import React, { useEffect, useState } from "react";

function ServiceHistory() {
    const [serviceList, setServiceList] = useState([])
    const [inputText, setInputText] = useState('')

    const getServiceList = async () => {
        const url = "http://localhost:8080/api/appointments/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setServiceList(data.appointments)
        }
    }

    function getDate(date) {
        return new Date(date).toLocaleDateString()
    }

    function getTime(time) {
        return new Date(time).toLocaleTimeString()
    }


    useEffect(() => {
        getServiceList();
    }, []);

    const handleInputChange = (event) => {
        const input = event.target.value.toUpperCase()
        setInputText(input)
    }

    return (
        <div className="container">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h1>Service History</h1>
            </div>
            <div className="input-group mb-3">
                <input value={inputText} onChange={handleInputChange} type="text" className="form-control" placeholder="Search by VIN..." aria-label="Search by VIN" aria-describedby="button-addon2" />
                    <button className="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
            </div>

            <table
                className="table table-bordered table-striped"
            >
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {serviceList.filter((service) => service.vin.startsWith(inputText)).map((service) => (
                        <tr key={service.id}>
                            <td>{service.vin}</td>
                            <td>{service.vip ? "Yes" : "No"}</td>
                            <td>{service.customer}</td>
                            <td>{getDate(service.date_time)}</td>
                            <td>{getTime(service.date_time)}</td>
                            <td>{`${service.technician.first_name} ${service.technician.last_name}`}</td>
                            <td>{service.reason}</td>
                            <td>{service.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div >
    );
}









export default ServiceHistory