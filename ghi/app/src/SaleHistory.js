import React, { useEffect, useState } from "react";

function SaleHistory() {
  const [sales, setSalesList] = useState([]);
  const [selectedSalespersonId, setSelectedSalespersonId] = useState('');
  const [salespersons, setSalesPersons] = useState([]);

  const handleSalesPeopleChange = (event) => {
    const value = event.target.value;
    setSelectedSalespersonId(value);
  }

  const SalesData = async () => {
    const response = await fetch("http://localhost:8091/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSalesList(data.sales);
    }
  }

  const SalespeopleData = async () => {
    const response = await fetch("http://localhost:8091/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespersons);
    }
  }

  useEffect(() => {
    SalesData();
    SalespeopleData();
  }, []);

  const filteredSales = sales.filter(sale =>  {
    return sale.salesperson.id  == selectedSalespersonId
  })

 
  return (
    <>
      <h1>Sales History</h1>
      <div className="form-floating mb-3">
        <select onChange={handleSalesPeopleChange} value={selectedSalespersonId} name="sales_person" id="sales_person" className="form-select">
          <option value="" >Choose a salesperson</option>
          {salespersons.map(salesperson => (
            <option key={salesperson.id} value={salesperson.id}>
              {salesperson.first_name} {salesperson.last_name}
            </option>
          ))}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
        
        {filteredSales.map(sale => (
                
            <tr key={sale.id}>
              <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default SaleHistory;
