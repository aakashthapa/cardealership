import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"

function ServiceForm() {

    // const date = ''
    // const time = ''
    // const datetime = date + " " + time

    const initialData = {
        vin: '',
        customer: '',
        date: '',
        time: '',
        reason: '',
        technician: '',
    }

    const [formData, setFormData] = useState(initialData)
    const [techs, setTechs] = useState([])
    const navigate = useNavigate()


    const handleFormChange = (event) => {
        const key = event.target.name
        let value = event.target.value
        if (key === "vin") {
            value = value.toUpperCase()
        }
        setFormData({
            ...formData,
            [key]: value,
        })
    }

    const fetchTechs = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechs(data.techs)
        }
    }

    useEffect(() => {
        fetchTechs();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            navigate("/serviceappointments")
        }
        
    }

    return (
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={formData.vin} onChange={handleFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.customer} onChange={handleFormChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.date} onChange={handleFormChange} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.time} onChange={handleFormChange} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.technician} onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {techs.map(tech => {
                                    return (
                                        <option key={tech.id} value={tech.id}>
                                            {tech.first_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Reason</label>
                            <textarea onChange={handleFormChange} value={formData.reason} className="form-control" id="reason" rows="3" name="reason"></textarea>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ServiceForm