import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import VehicleModel from './VehicleModel';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonList from './SalesPersonList';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SaleHistory from './SaleHistory';
import ServiceForm from './ServiceForm';
import ServiceList from './ServiceList';
import ServiceHistory from './ServiceHistory'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers" >
            <Route path="" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" >
            <Route path="" element ={<VehicleModel />} />
            <Route path="new" element ={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles" >
            <Route path="" element ={<AutomobileList />} />
            <Route path="new" element ={<AutomobileForm />} />
          </Route>

          <Route path="technicianlist">
            <Route path="" element={<TechnicianList />} />
            <Route path="addtech" element={<TechnicianForm />} />
          </Route>

          <Route path="salesperson" >
            <Route path="" element={<SalesPersonList  />} />
            <Route path="new" element ={<SalesPersonForm />} />
          </Route>

          <Route path="customer" >
            <Route path="" element={<CustomerList  />} />
            <Route path="new" element={<CustomerForm  />} />
          </Route>

          <Route path="sale" >
            <Route path="" element={<SalesList  />} />
            <Route path="new" element={<SalesForm  />} />
            <Route path="history" element={<SaleHistory />} />
          </Route>

          <Route path="serviceappointments">
            <Route path="" element={<ServiceList />} />
            <Route path="createappointment" element={<ServiceForm />} />
            <Route path="servicehistory" element={<ServiceHistory />} />
          </Route>

        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
