import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom"

function SalesForm() {
  const [salesperson, setSalesperson] = useState('');
  const [salesPeople, setSalesPeople] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [auto, setAuto] = useState('');
  const [autos, setAutos] = useState([]);
  const [price, setPrice] = useState('');
  const [successMessage, setSuccessMessage] = useState('');
  const navigate = useNavigate();

  const handlePeopleChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleVinChange = (event) => {
    const value = event.target.value;
    setAuto(value);
  }

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      salesperson: salesperson,
      customer: customer,
      automobile: auto,
      price: price
    };

    try {
      const SalesUrl = "http://localhost:8091/api/sales/";
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const putUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
      const putConfig = {
        method: 'PUT',
        body: JSON.stringify({ sold: true }),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      await fetch(putUrl, putConfig);

      const response = await fetch(SalesUrl, fetchConfig);
      if (response.ok) {
        setSuccessMessage('Sale created successfully!');
        setSalesperson('');
        setCustomer('');
        setAuto('');
        setPrice('');
        navigate("/sale")
      } else {
        throw new Error('Failed to create sale');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const fetchAutos = async () => {
    try {
      const Url = 'http://localhost:8100/api/automobiles/';
      const response = await fetch(Url);
      if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
      } else {
        throw new Error('Failed to fetch automobiles');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const fetchSalespeople = async () => {
    try {
      const response = await fetch("http://localhost:8091/api/salespeople/");
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.salespersons);
      } else {
        throw new Error('Failed to fetch salespeople');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  const fetchCustomers = async () => {
    try {
      const response = await fetch("http://localhost:8091/api/customers/");
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      } else {
        throw new Error('Failed to fetch customers');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  useEffect(() => {
    fetchSalespeople();
    fetchCustomers();
    fetchAutos();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2 className="display-5 text-center"><b>Record a sale</b></h2>
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit} id="Record a sale">
            <div className="form-floating mb-3">
              <select onChange={handleVinChange} value={auto} required type="" id="vin" className="form-select">
                <option value="">Choose an automobile VIN</option>
                {autos.filter(auto => !auto.sold)
                  .map(auto => {
                    return (
                      <option key={auto.id} value={auto.vin}>
                        {auto.vin}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <select onChange={handlePeopleChange} value={salesperson} name="sales_person" id="sales_person" className="form-select">
                <option value="">Choose a salesperson</option>
                {salesPeople?.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <select onChange={handleCustomerChange} value={customer} required name="" id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers?.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePriceChange} placeholder="price" required type="text" name="price" id="price" value={price} className="form-control" />
              <label htmlFor="color">Price</label>
            </div>
            <button className="btn btn-outline-dark">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default SalesForm;
