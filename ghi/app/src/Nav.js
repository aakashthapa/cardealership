import { NavLink } from 'react-router-dom';

function Nav() {
  let activeClassName = "nav-link active"
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="manufacturers" end>Manufactures</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="manufacturers/new" end>Create a Manufacturer</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="models" end>Models</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="models/new" end>Create a Model</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="automobiles" end>Automobiles</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="automobiles/new" end>Create an Automobile</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="salesperson" end>Salespeople</NavLink>
              </li>
              <li>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="salesperson/new" end>Add a Salesperson</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="customer" end>Customers</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>


      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="customer/new" end>Add a Customer</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="sale" end>Sales</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="sale/new" end>Add a Sale</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="sale/history" end>Sales History</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="technicianlist" end>Technicians</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="technicianlist/addtech" end>Add a Technician</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="serviceappointments" end>Service Appointments</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="serviceappointments/createappointment" end>Create a Service Appointment</NavLink>
              </li>
              <li className='nav-item'>
                <NavLink className={({ isActive }) =>
                  isActive ? activeClassName : "nav-link"} to="serviceappointments/servicehistory" end>Service History</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Nav;
